#!groovy​
// This list contains all of the directories that have containers we want to build. If you add a new container, all
// you should have to do is add it to this list
def toBeProcessed = ['./container1', './container2']

// This is referenced in the compose files and used to tag and deploy images
env.DOCKER_TAG = env.BUILD_NUMBER

// Creates a docker build step for the given container dir
def makeBuildSteps = { String containerDir ->
    return {
        echo "building: " + containerDir
        bat "cd ${containerDir} && docker-compose build"
    }
}

// Makes a docker upload step for the given container dir
def makeUploadSteps = { String containerDir ->
    return {
        echo "uploading: " + containerDir
        withCredentials([usernamePassword(credentialsId: 'docker-hub-credentials', passwordVariable: 'password', usernameVariable: 'username')]) {
            bat "cd ${containerDir} && docker login -u $username -p $password"
            bat "cd ${containerDir} && docker-compose push" 
        } 
    }
}

/**
 * Makes all of the steps needed to execute a command on all of the containers.
 * @param containers: the array of containers we are making the steps for.
 * @param c: the clousure that builds the step we want to invoke on each container
 * @return an mapping of step numbers to the command that should be executed.
 */
def makeStepsForAllDirs(containers, Closure c) {
    def steps = [:]
    for(int i = 0 ; i < containers.size() ; i++) {
        def dir = containers.get(i)
        def deployStep = "${dir}"
        steps[deployStep] = c(dir)
    }
    return steps
}

/**
 * Gets the last known good build.
 * @return the number of the last known good build or -1 if one could not be found.
 */
def getLastSuccessfulBuild() {
    def toCheck = currentBuild.previousBuild
    while(toCheck != null) {
        if(toCheck.result == 'SUCCESS') {
            return toCheck.number
        }
        toCheck = toCheck.previousBuild
    }
    return -1
}

def triggerRemoteUpdate(String buildNumner) {
    def post = new URL("http://SHADLAB10-33099:3000/buildproxy/multiteststack/" + buildNumner).openConnection();
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Content-Type", "application/json")
    def postRC = post.getResponseCode();
    return postRC == 201
}

node {
    def app

    stage('Clone repository') {
        checkout scm
    }

    stage('Build and test image') {
        parallel makeStepsForAllDirs(toBeProcessed, makeBuildSteps)
    }

    // For testing we're just pushing to docker hub
    stage('Push image') {
        echo "Building with tag: ${env.DOCKER_TAG}"
        parallel makeStepsForAllDirs(toBeProcessed, makeUploadSteps)
    }

    // This will redeploy the stack with the latest build
    stage('Deploy Updates') {
        triggerRemoteUpdate(env.DOCKER_TAG)
        // bat 'docker stack deploy -c docker-stack.yml multiteststack'
        // It takes a little bit for the stack to redeploy. There should be a better way of doing this
        sleep 25
    }

    // stage('Performing integration tests') {
    //     try {
    //         // make sure we're good!
    //         bat 'npm install && npm run inttest'
    //     } catch (error) {
    //         echo 'I have an error'
    //         // Revert to the last known good build
    //         // TODO this does not handle the "I don't know the last build" case.... we should
    //         env.DOCKER_TAG = getLastSuccessfulBuild()
    //         echo "Tests did not pass. Rolling stack back to build: ${env.DOCKER_TAG}"
    //         bat 'docker stack deploy -c docker-stack.yml multiteststack'
    //         throw error
    //     }
    // }
}